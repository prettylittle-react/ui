import PropTypes from 'prop-types';

import Icon from 'icon';
import IconCross from 'icon/cross.svg';

import Ui from './Ui';

/**
 * UiClose
 * @description [Description]
 * @example
  <div id="UiClose"></div>
  <script>
    ReactDOM.render(React.createElement(Components.UiClose, {
    	title : 'Example Close'
    }), document.getElementById("UiClose"));
  </script>
 */
const UiClose = props => {
	return (
		<Ui {...props}>
			<Icon icon={IconCross} />
		</Ui>
	);
};

UiClose.defaultProps = {
	children: null
};

UiClose.propTypes = {
	children: PropTypes.node
};

export default UiClose;
