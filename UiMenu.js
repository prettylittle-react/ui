import PropTypes from 'prop-types';

import Ui from './Ui';

import './UiMenu.scss';

/**
 * UiMenu
 * @description [Description]
 * @example
  <div id="Close"></div>
  <script>
    ReactDOM.render(React.createElement(Components.UiMenu, {
    	title : 'Example UiMenu'
    }), document.getElementById("UiMenu"));
  </script>
 */
class UiMenu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: props.isOpen
		};

		this.baseClass = 'ui';
	}

	onClick = ev => {};

	componentWillReceiveProps(nextProps) {
		const {isOpen} = this.state;

		if ((nextProps.isOpen && isOpen === false) || (nextProps.isOpen === false && isOpen)) {
			this.setState({isOpen: nextProps.isOpen});
		}
	}

	render() {
		const {isOpen} = this.state;

		return (
			<Ui {...this.props}>
				<svg
					className={`icon icon-menu-toggle ${isOpen ? 'opened' : ''}`}
					aria-hidden="true"
					x="0px"
					y="0px"
					viewBox="0 0 100 100"
				>
					<g className="svg-menu-toggle">
						<path className="line line-1" d="M5 13h90v14H5z" />
						<path className="line line-2" d="M5 43h90v14H5z" />
						<path className="line line-3" d="M5 73h90v14H5z" />
					</g>
				</svg>
			</Ui>
		);
	}
}

UiMenu.defaultProps = {
	isOpen: false,
	children: null
};

UiMenu.propTypes = {
	children: PropTypes.node
};

export default UiMenu;
