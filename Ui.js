import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Ui.scss';

/**
 * Ui
 * @description [Description]
 * @example
  <div id="Ui"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Ui, {
    	title : 'Example Ui'
    }), document.getElementById("Ui"));
  </script>
 */
class Ui extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'ui';
	}

	render() {
		const {title, onClick, label, children, className} = this.props;

		const atts = {
			title,
			className: this.baseClass
		};

		if (className) {
			atts.className += ` ${className}`;
		}

		if (onClick) {
			atts.onClick = onClick;
		}

		if (children && label) {
			atts.title = label;
		}

		return <button {...atts}>{children ? children : <Text content={label} />}</button>;
	}
}

Ui.defaultProps = {
	title: undefined,
	label: '',
	className: '',
	onClick: null,
	children: null
};

Ui.propTypes = {
	title: PropTypes.string,
	label: PropTypes.string,
	className: PropTypes.string,
	onClick: PropTypes.func,
	children: PropTypes.node
};

export default Ui;
